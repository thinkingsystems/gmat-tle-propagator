# GMAT TLE Propagator

NASA's General Mission Analysis Tool, GMAT, lacks a TLE propagation component.  This project provides a TLE propagator for GMAT users.

Note that this prototype has been incorporated into GMAT as of R2022a.  The project here is no longer active.
